#include "okto_command_filter.h"

OktoCommandFilter::OktoCommandFilter(int droneId) :
    enable_ctrl_roll_( false),
    enable_ctrl_pitch_(false),
    enable_ctrl_yaw_(  false),
    max_ctrl_thrust_(255),
    min_ctrl_thrust_(  0),
    max_ctrl_roll_(  127),
    max_ctrl_pitch_( 127),
    max_ctrl_yaw_(   127)
{
    initializeParams(droneId);
}

void OktoCommandFilter::open(ros::NodeHandle &nIn, std::string moduleName)
{
}

void OktoCommandFilter::initializeParams(int droneId)
{
    std::cout << "entering OktoCommandFilter::initializeParams()" << std::endl;

    std::string stackPath;
    ros::param::get("~stackPath",stackPath);
    stackPath+="/";
    try {
        XMLFileReader my_xml_reader(stackPath+"configs/drone"+ cvg_int_to_string(droneId)+"/driver_okto.xml");
        // Configuration parameters
        enable_ctrl_yaw_    = (bool) my_xml_reader.readIntValue( "driver_okto_config:enable_ctrl_yaw"    );
        enable_ctrl_roll_   = (bool) my_xml_reader.readIntValue( "driver_okto_config:enable_ctrl_roll"   );
        enable_ctrl_pitch_  = (bool) my_xml_reader.readIntValue( "driver_okto_config:enable_ctrl_pitch"  );
        // saturation of okto commands (min/max output - in asctec units)
        max_ctrl_thrust_ = my_xml_reader.readIntValue( "driver_okto_config:max_ctrl_thrust" );
        min_ctrl_thrust_ = my_xml_reader.readIntValue( "driver_okto_config:min_ctrl_thrust" );
        max_ctrl_roll_   = my_xml_reader.readIntValue( "driver_okto_config:max_ctrl_roll"   );
        max_ctrl_pitch_  = my_xml_reader.readIntValue( "driver_okto_config:max_ctrl_pitch"  );
        max_ctrl_yaw_    = my_xml_reader.readIntValue( "driver_okto_config:max_ctrl_yaw"    );
    } catch ( cvg_XMLFileReader_exception &e) {
    throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }

    std::cout << "OktoCommandFilter configuration parameters:" << std::endl;
    std::cout << "enable_ctrl_yaw_   :" << enable_ctrl_yaw_    << std::endl;
    std::cout << "enable_ctrl_roll_  :" << enable_ctrl_roll_   << std::endl;
    std::cout << "enable_ctrl_pitch_ :" << enable_ctrl_pitch_  << std::endl;
    std::cout << "max_ctrl_thrust_:" << max_ctrl_thrust_ << std::endl;
    std::cout << "min_ctrl_thrust_:" << min_ctrl_thrust_ << std::endl;
    std::cout << "max_ctrl_roll_  :" << max_ctrl_roll_   << std::endl;
    std::cout << "max_ctrl_pitch_ :" << max_ctrl_pitch_  << std::endl;
    std::cout << "max_ctrl_yaw_   :" << max_ctrl_yaw_    << std::endl;

    std::cout << "exiting  OktoCommandFilter::initializeParams()" << std::endl;
}

#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void OktoCommandFilter::assembleCtrlCommands(okto_driver::OktoCommand &msg)
{
    // command saturation
    KEEP_IN_RANGE( msg.gas ,  min_ctrl_thrust_,  max_ctrl_thrust_);
    KEEP_IN_RANGE( msg.dyaw, -max_ctrl_yaw_   , +max_ctrl_yaw_   );
    KEEP_IN_RANGE( msg.roll, -max_ctrl_roll_  , +max_ctrl_roll_  );
    KEEP_IN_RANGE( msg.nick, -max_ctrl_pitch_ , +max_ctrl_pitch_ );

    // ctrl mask (enabling of control channels)
    if (!enable_ctrl_yaw_)    msg.dyaw = 0;
    if (!enable_ctrl_roll_)   msg.roll = 0;
    if (!enable_ctrl_pitch_)  msg.nick = 0;
}
