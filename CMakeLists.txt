cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME driverOktoROSModule)
project(${PROJECT_NAME})

### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake

#add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
#add_definitions(-std=c++0x)
add_definitions(-std=c++03)



# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)



set(OKTODRIVER_SOURCE_DIR
	src)
	
set(OKTODRIVER_INCLUDE_DIR
	src/include
	)

set(OKTODRIVER_SOURCE_FILES
        src/source/oktoOuts.cpp
        src/source/oktoInps.cpp
        src/source/okto_command_filter.cpp
	)
	
set(OKTODRIVER_HEADER_FILES
        src/include/oktoOuts.h
        src/include/oktoInps.h
        src/include/okto_command_filter.h
	)

set(JOY_CONVERTER_SOURCE_FILES
        src/source/joy2cvgstack/joyconverterdrvoktocmd.cpp
        src/source/joy2cvgstack/joyconverterdrvoktocmdNode.cpp
        )

set(JOY_CONVERTER_HEADER_FILES
        src/include/joy2cvgstack/joyconverterdrvoktocmd.h
        )



find_package(catkin REQUIRED
                COMPONENTS roscpp std_msgs geometry_msgs sensor_msgs okto_driver droneModuleROS droneMsgsROS lib_cvgutils)



catkin_package(
#        INCLUDE_DIRS ${OKTODRIVER_INCLUDE_DIR}
        CATKIN_DEPENDS roscpp std_msgs geometry_msgs sensor_msgs okto_driver droneModuleROS droneMsgsROS lib_cvgutils
  )


include_directories(${OKTODRIVER_INCLUDE_DIR})
include_directories(${catkin_INCLUDE_DIRS})


add_library(driverOktoROSModule ${OKTODRIVER_SOURCE_FILES} ${OKTODRIVER_HEADER_FILES})
add_dependencies(driverOktoROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(driverOktoROSModule ${catkin_LIBRARIES})


add_executable(batteryOktoROSModule src/source/batteryNode.cpp)
add_dependencies(batteryOktoROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(batteryOktoROSModule driverOktoROSModule)
target_link_libraries(batteryOktoROSModule ${catkin_LIBRARIES})

add_executable(rotationAnglesOktoROSModule src/source/rotationAnglesNode.cpp)
add_dependencies(rotationAnglesOktoROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(rotationAnglesOktoROSModule driverOktoROSModule)
target_link_libraries(rotationAnglesOktoROSModule ${catkin_LIBRARIES})

add_executable(droneCommandOktoROSModule   src/source/droneCommandNode.cpp)
add_dependencies(droneCommandOktoROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneCommandOktoROSModule driverOktoROSModule)
target_link_libraries(droneCommandOktoROSModule ${catkin_LIBRARIES})

add_executable(joy_converter_to_drv_okto_cmd ${JOY_CONVERTER_SOURCE_FILES} ${JOY_CONVERTER_HEADER_FILES})
add_dependencies(joy_converter_to_drv_okto_cmd ${catkin_EXPORTED_TARGETS})
target_link_libraries(joy_converter_to_drv_okto_cmd ${catkin_LIBRARIES})
